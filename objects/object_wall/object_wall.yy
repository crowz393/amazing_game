{
    "id": "089b2483-84ef-41f5-8133-7038d9627259",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_wall",
    "eventList": [
        {
            "id": "c043b844-bbc4-4630-bde0-0e1812c0836e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "089b2483-84ef-41f5-8133-7038d9627259"
        },
        {
            "id": "32a4a9d7-4231-4d68-8289-b1d048e27feb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "5dce8503-7ed8-4def-baea-01c2fefbd3b0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "089b2483-84ef-41f5-8133-7038d9627259"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "d0431fee-da19-47a8-955d-73f8611dd054",
    "visible": true
}