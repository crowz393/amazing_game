{
    "id": "5dce8503-7ed8-4def-baea-01c2fefbd3b0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_player",
    "eventList": [
        {
            "id": "fcd2110c-bccf-4f35-9542-b11d1fc6415a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5dce8503-7ed8-4def-baea-01c2fefbd3b0"
        },
        {
            "id": "c038fc2f-7a4c-4f16-85c5-464089e9abe7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "5dce8503-7ed8-4def-baea-01c2fefbd3b0"
        },
        {
            "id": "118ad787-7d8b-40fd-83e1-5e5400b0ca06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "5dce8503-7ed8-4def-baea-01c2fefbd3b0"
        },
        {
            "id": "24943ff3-aadb-4a88-9b25-71af50e63b12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "5dce8503-7ed8-4def-baea-01c2fefbd3b0"
        },
        {
            "id": "dba63854-db35-45e5-9c86-0c6a26e42730",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "5dce8503-7ed8-4def-baea-01c2fefbd3b0"
        },
        {
            "id": "fec28d52-449f-49b2-a97f-df75b37c727c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 5,
            "m_owner": "5dce8503-7ed8-4def-baea-01c2fefbd3b0"
        },
        {
            "id": "6fedf892-0eed-4d91-ad1d-5aff13de664c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "5dce8503-7ed8-4def-baea-01c2fefbd3b0"
        },
        {
            "id": "d37953db-d5b1-47b7-a9b0-3567ae31d20b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 5,
            "m_owner": "5dce8503-7ed8-4def-baea-01c2fefbd3b0"
        },
        {
            "id": "0f99a42e-1590-4af6-9b9d-0e4b416ecf62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "089b2483-84ef-41f5-8133-7038d9627259",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5dce8503-7ed8-4def-baea-01c2fefbd3b0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "a972cb30-b248-4c3a-8abe-35ee238b686d",
    "visible": true
}