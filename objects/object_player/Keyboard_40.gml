/// @DnDAction : YoYo Games.Movement.Set_Direction_Fixed
/// @DnDVersion : 1.1
/// @DnDHash : 03D9A044
/// @DnDArgument : "direction" "270"
direction = 270;

/// @DnDAction : YoYo Games.Instances.Sprite_Scale
/// @DnDVersion : 1
/// @DnDHash : 65193802
/// @DnDArgument : "xscale" "-1"
image_xscale = -1;
image_yscale = 1;

/// @DnDAction : YoYo Games.Movement.Set_Speed
/// @DnDVersion : 1
/// @DnDHash : 1006323C
/// @DnDArgument : "speed" "4"
speed = 4;

/// @DnDAction : YoYo Games.Instances.Sprite_Animation_Speed
/// @DnDVersion : 1
/// @DnDHash : 2B0F449B
image_speed = 1;

/// @DnDAction : YoYo Games.Instances.Set_Sprite
/// @DnDVersion : 1
/// @DnDHash : 46B6E29A
/// @DnDArgument : "imageind" "image_index"
/// @DnDArgument : "spriteind" "sprite_player_up"
/// @DnDSaveInfo : "spriteind" "123ca371-818d-4c1b-88a0-46945abf3ee3"
sprite_index = sprite_player_up;
image_index = image_index;