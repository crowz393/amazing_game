{
    "id": "d0431fee-da19-47a8-955d-73f8611dd054",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_block",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "854f40c0-eda8-4d6f-a4e7-178604bb50d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0431fee-da19-47a8-955d-73f8611dd054",
            "compositeImage": {
                "id": "a5b9f3d4-93a9-474d-8da1-9571c8b52ed0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "854f40c0-eda8-4d6f-a4e7-178604bb50d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2941a548-f637-4958-adf1-b4e0132273f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "854f40c0-eda8-4d6f-a4e7-178604bb50d1",
                    "LayerId": "a9f84f49-181b-4bc6-9f1d-2deb2c9c52d2"
                }
            ]
        },
        {
            "id": "b9bfff82-bc23-491c-9a02-7f71ca670850",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0431fee-da19-47a8-955d-73f8611dd054",
            "compositeImage": {
                "id": "4536859b-a21c-495b-89cb-98aca35f8ec2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9bfff82-bc23-491c-9a02-7f71ca670850",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff4f00f7-eb93-4c18-8a17-f6f95bc8d648",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9bfff82-bc23-491c-9a02-7f71ca670850",
                    "LayerId": "a9f84f49-181b-4bc6-9f1d-2deb2c9c52d2"
                }
            ]
        },
        {
            "id": "a890f41f-3aae-48b2-9087-e28e50b935b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0431fee-da19-47a8-955d-73f8611dd054",
            "compositeImage": {
                "id": "2afd2f41-1780-4946-bfaf-a35c0e0f7546",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a890f41f-3aae-48b2-9087-e28e50b935b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "347a0be5-1738-43f5-be6b-dceeac9230e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a890f41f-3aae-48b2-9087-e28e50b935b1",
                    "LayerId": "a9f84f49-181b-4bc6-9f1d-2deb2c9c52d2"
                }
            ]
        },
        {
            "id": "39403d30-2f89-46d0-9f15-2227b2fba754",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0431fee-da19-47a8-955d-73f8611dd054",
            "compositeImage": {
                "id": "5113f292-8c1a-488c-a140-f233f5a56077",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39403d30-2f89-46d0-9f15-2227b2fba754",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "971fe186-72f3-4050-89e8-40f2b779947f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39403d30-2f89-46d0-9f15-2227b2fba754",
                    "LayerId": "a9f84f49-181b-4bc6-9f1d-2deb2c9c52d2"
                }
            ]
        },
        {
            "id": "172fdd95-9c8a-4a00-9817-0d4150f5b56f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0431fee-da19-47a8-955d-73f8611dd054",
            "compositeImage": {
                "id": "6ac51620-2feb-4627-a14f-cba2924a2008",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "172fdd95-9c8a-4a00-9817-0d4150f5b56f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e13fdc9c-e8ac-4b48-819a-dcedc3f35c2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "172fdd95-9c8a-4a00-9817-0d4150f5b56f",
                    "LayerId": "a9f84f49-181b-4bc6-9f1d-2deb2c9c52d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a9f84f49-181b-4bc6-9f1d-2deb2c9c52d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d0431fee-da19-47a8-955d-73f8611dd054",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}