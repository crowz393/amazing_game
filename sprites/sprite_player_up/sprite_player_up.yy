{
    "id": "123ca371-818d-4c1b-88a0-46945abf3ee3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a810268f-0fe0-410e-ad49-ac168d1d319a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "123ca371-818d-4c1b-88a0-46945abf3ee3",
            "compositeImage": {
                "id": "2fe59627-6482-49fc-85bb-255ddd395963",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a810268f-0fe0-410e-ad49-ac168d1d319a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01e4d1b5-ad4d-4318-8913-3f33abde89f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a810268f-0fe0-410e-ad49-ac168d1d319a",
                    "LayerId": "2ab74724-78fa-4d8f-ad05-b355ffeffca8"
                }
            ]
        },
        {
            "id": "f280808e-69db-486f-9407-a2e3f4008ee7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "123ca371-818d-4c1b-88a0-46945abf3ee3",
            "compositeImage": {
                "id": "c654a61c-8997-4615-9a47-202dd763f1c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f280808e-69db-486f-9407-a2e3f4008ee7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "571aa48b-8a53-43fe-b87c-7c4def2f9b6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f280808e-69db-486f-9407-a2e3f4008ee7",
                    "LayerId": "2ab74724-78fa-4d8f-ad05-b355ffeffca8"
                }
            ]
        },
        {
            "id": "8addee83-cf9e-4b08-844d-4a24e7be042d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "123ca371-818d-4c1b-88a0-46945abf3ee3",
            "compositeImage": {
                "id": "e9ee1108-31c5-45ad-870e-828626e46918",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8addee83-cf9e-4b08-844d-4a24e7be042d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c676e03-cc48-40fe-9547-eb42939527f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8addee83-cf9e-4b08-844d-4a24e7be042d",
                    "LayerId": "2ab74724-78fa-4d8f-ad05-b355ffeffca8"
                }
            ]
        },
        {
            "id": "e31b290e-aede-42fe-81bb-4d2566314f1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "123ca371-818d-4c1b-88a0-46945abf3ee3",
            "compositeImage": {
                "id": "c855eb9e-9c38-4d07-9eef-b1e0f6a11ea4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e31b290e-aede-42fe-81bb-4d2566314f1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "312f2ea0-cc02-4961-bc2b-0eb7720d6035",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e31b290e-aede-42fe-81bb-4d2566314f1a",
                    "LayerId": "2ab74724-78fa-4d8f-ad05-b355ffeffca8"
                }
            ]
        },
        {
            "id": "81fa1044-b0c3-43f8-94d9-556dd9532683",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "123ca371-818d-4c1b-88a0-46945abf3ee3",
            "compositeImage": {
                "id": "4a030480-260b-49a7-8e06-ea4c4334d9cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81fa1044-b0c3-43f8-94d9-556dd9532683",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de3ea316-cff8-435a-91e8-0704b0426f27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81fa1044-b0c3-43f8-94d9-556dd9532683",
                    "LayerId": "2ab74724-78fa-4d8f-ad05-b355ffeffca8"
                }
            ]
        },
        {
            "id": "b5c838bb-2345-42f1-a31d-77c79e2b2fdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "123ca371-818d-4c1b-88a0-46945abf3ee3",
            "compositeImage": {
                "id": "fbc4a725-196f-4400-92af-af0ded9417b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5c838bb-2345-42f1-a31d-77c79e2b2fdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4a1b261-b657-4e43-b6e5-1415c172804c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5c838bb-2345-42f1-a31d-77c79e2b2fdb",
                    "LayerId": "2ab74724-78fa-4d8f-ad05-b355ffeffca8"
                }
            ]
        },
        {
            "id": "28560227-fc9a-421e-a664-5a6d7c322068",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "123ca371-818d-4c1b-88a0-46945abf3ee3",
            "compositeImage": {
                "id": "1c6a1530-f2df-4400-bce3-af3110f19b39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28560227-fc9a-421e-a664-5a6d7c322068",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4f209f1-af0c-446c-851d-b4c596d9a3b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28560227-fc9a-421e-a664-5a6d7c322068",
                    "LayerId": "2ab74724-78fa-4d8f-ad05-b355ffeffca8"
                }
            ]
        },
        {
            "id": "e15c8fa2-659d-4483-8947-7f25663b0d22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "123ca371-818d-4c1b-88a0-46945abf3ee3",
            "compositeImage": {
                "id": "ca2da587-58b3-43c7-b5a5-50c94db9ab05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e15c8fa2-659d-4483-8947-7f25663b0d22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "402fc34e-86df-4938-914c-d8cfaa6629ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e15c8fa2-659d-4483-8947-7f25663b0d22",
                    "LayerId": "2ab74724-78fa-4d8f-ad05-b355ffeffca8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2ab74724-78fa-4d8f-ad05-b355ffeffca8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "123ca371-818d-4c1b-88a0-46945abf3ee3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}