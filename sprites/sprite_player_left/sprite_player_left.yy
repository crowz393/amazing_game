{
    "id": "582eec0d-24af-4b55-91fe-26449fa34b9a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33b84d68-f795-4d92-978a-701d49bfb697",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "582eec0d-24af-4b55-91fe-26449fa34b9a",
            "compositeImage": {
                "id": "409fc553-b602-4354-bef4-6426bed5c9a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33b84d68-f795-4d92-978a-701d49bfb697",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d4bc636-5fde-46d4-8b67-95abd234a559",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33b84d68-f795-4d92-978a-701d49bfb697",
                    "LayerId": "e62d3b32-5283-464f-a7c3-9a3dcf0f06fe"
                }
            ]
        },
        {
            "id": "d964df3f-f678-4a69-933b-c63f966ed21e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "582eec0d-24af-4b55-91fe-26449fa34b9a",
            "compositeImage": {
                "id": "b7c07281-8eaf-41fb-8692-4b153230aa72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d964df3f-f678-4a69-933b-c63f966ed21e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0c57ad1-a161-4fa6-9a8d-e8a4662b8218",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d964df3f-f678-4a69-933b-c63f966ed21e",
                    "LayerId": "e62d3b32-5283-464f-a7c3-9a3dcf0f06fe"
                }
            ]
        },
        {
            "id": "9a0e4d22-1ab3-4842-8923-b28fe605f744",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "582eec0d-24af-4b55-91fe-26449fa34b9a",
            "compositeImage": {
                "id": "67a4452c-541d-45bb-bd4b-975b2bfc27e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a0e4d22-1ab3-4842-8923-b28fe605f744",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e12be1ca-6b3d-4817-893b-7bdcf2eb53b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a0e4d22-1ab3-4842-8923-b28fe605f744",
                    "LayerId": "e62d3b32-5283-464f-a7c3-9a3dcf0f06fe"
                }
            ]
        },
        {
            "id": "c77ed44f-8bbb-4f35-af9b-4731c39b53d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "582eec0d-24af-4b55-91fe-26449fa34b9a",
            "compositeImage": {
                "id": "22d0165a-619b-41a1-8911-9292638bc29f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c77ed44f-8bbb-4f35-af9b-4731c39b53d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4abf54f7-3ec7-4f00-aa62-5b8c8ed91e88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c77ed44f-8bbb-4f35-af9b-4731c39b53d5",
                    "LayerId": "e62d3b32-5283-464f-a7c3-9a3dcf0f06fe"
                }
            ]
        },
        {
            "id": "f61e0526-5d04-4256-83f2-0079b72efd1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "582eec0d-24af-4b55-91fe-26449fa34b9a",
            "compositeImage": {
                "id": "8a8dce96-b935-4c6b-8a62-7aa6907e877e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f61e0526-5d04-4256-83f2-0079b72efd1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae564ef7-5f2d-47b6-a69b-4b896b0bd64e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f61e0526-5d04-4256-83f2-0079b72efd1d",
                    "LayerId": "e62d3b32-5283-464f-a7c3-9a3dcf0f06fe"
                }
            ]
        },
        {
            "id": "007e2ef8-8b3d-42ea-8364-39856b0550f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "582eec0d-24af-4b55-91fe-26449fa34b9a",
            "compositeImage": {
                "id": "6129280b-e0d4-41fb-ab97-02d26e3088ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "007e2ef8-8b3d-42ea-8364-39856b0550f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52d87f73-3604-4eb8-b566-35c528fc62fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "007e2ef8-8b3d-42ea-8364-39856b0550f8",
                    "LayerId": "e62d3b32-5283-464f-a7c3-9a3dcf0f06fe"
                }
            ]
        },
        {
            "id": "4d8d7362-828f-4979-a7d5-9169faa966b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "582eec0d-24af-4b55-91fe-26449fa34b9a",
            "compositeImage": {
                "id": "a8c5ba6e-dc8d-47b3-bc58-c82368da45c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d8d7362-828f-4979-a7d5-9169faa966b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d44b9784-b482-4ef6-a307-5b97d005d715",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d8d7362-828f-4979-a7d5-9169faa966b3",
                    "LayerId": "e62d3b32-5283-464f-a7c3-9a3dcf0f06fe"
                }
            ]
        },
        {
            "id": "2b3e6b56-36d9-49ed-9b69-96307f4def41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "582eec0d-24af-4b55-91fe-26449fa34b9a",
            "compositeImage": {
                "id": "d7b6c281-3fa7-438a-a2df-0c35d8b98bad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b3e6b56-36d9-49ed-9b69-96307f4def41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2c42fd4-0bb5-4e2e-b826-56f96576d01e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b3e6b56-36d9-49ed-9b69-96307f4def41",
                    "LayerId": "e62d3b32-5283-464f-a7c3-9a3dcf0f06fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e62d3b32-5283-464f-a7c3-9a3dcf0f06fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "582eec0d-24af-4b55-91fe-26449fa34b9a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}