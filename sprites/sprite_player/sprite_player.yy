{
    "id": "a972cb30-b248-4c3a-8abe-35ee238b686d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9fbbb847-adff-49ca-9bf9-9cc0a758bae5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a972cb30-b248-4c3a-8abe-35ee238b686d",
            "compositeImage": {
                "id": "e3b41b3a-0be4-49ca-ad6a-88003905d938",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fbbb847-adff-49ca-9bf9-9cc0a758bae5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c70ee915-956b-409d-92cc-58e153046d80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fbbb847-adff-49ca-9bf9-9cc0a758bae5",
                    "LayerId": "8f357d51-e4b1-4bce-b0ac-b7766d688767"
                }
            ]
        },
        {
            "id": "22a9c40b-ce0f-4467-a007-530d3c154f76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a972cb30-b248-4c3a-8abe-35ee238b686d",
            "compositeImage": {
                "id": "d0c8aa39-a0b2-4617-bfee-9d842dffe763",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22a9c40b-ce0f-4467-a007-530d3c154f76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e4a53ef-4d19-4790-ad53-ce194324d5eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22a9c40b-ce0f-4467-a007-530d3c154f76",
                    "LayerId": "8f357d51-e4b1-4bce-b0ac-b7766d688767"
                }
            ]
        },
        {
            "id": "00b19bed-ca1d-4cc7-ad83-c0670030275d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a972cb30-b248-4c3a-8abe-35ee238b686d",
            "compositeImage": {
                "id": "b1b528ae-0e98-4594-9184-def9f33bcc58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00b19bed-ca1d-4cc7-ad83-c0670030275d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d957f520-b891-441c-bb5f-614585d0e117",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00b19bed-ca1d-4cc7-ad83-c0670030275d",
                    "LayerId": "8f357d51-e4b1-4bce-b0ac-b7766d688767"
                }
            ]
        },
        {
            "id": "d51fd252-5f03-4646-b9f1-38c7b24723c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a972cb30-b248-4c3a-8abe-35ee238b686d",
            "compositeImage": {
                "id": "805bbedb-7b33-4b45-bd01-32dcf5c2e68b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d51fd252-5f03-4646-b9f1-38c7b24723c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a03013fa-9f86-4597-801e-ae50de5efe41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d51fd252-5f03-4646-b9f1-38c7b24723c8",
                    "LayerId": "8f357d51-e4b1-4bce-b0ac-b7766d688767"
                }
            ]
        },
        {
            "id": "df09bd2d-c2cf-44b0-86fb-92320907a259",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a972cb30-b248-4c3a-8abe-35ee238b686d",
            "compositeImage": {
                "id": "0819175c-f165-440e-b9b3-d68fe6f56a57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df09bd2d-c2cf-44b0-86fb-92320907a259",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6719a429-d576-4fd7-ab95-191b6a04b931",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df09bd2d-c2cf-44b0-86fb-92320907a259",
                    "LayerId": "8f357d51-e4b1-4bce-b0ac-b7766d688767"
                }
            ]
        },
        {
            "id": "41ef5e9d-66fd-4eef-9150-3dc54b635482",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a972cb30-b248-4c3a-8abe-35ee238b686d",
            "compositeImage": {
                "id": "4cab55d1-110f-4f75-8b41-58c859425e89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41ef5e9d-66fd-4eef-9150-3dc54b635482",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61c9165f-b80e-405e-927b-7189ac5f706a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41ef5e9d-66fd-4eef-9150-3dc54b635482",
                    "LayerId": "8f357d51-e4b1-4bce-b0ac-b7766d688767"
                }
            ]
        },
        {
            "id": "5036247b-b696-409b-913b-deee458d50de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a972cb30-b248-4c3a-8abe-35ee238b686d",
            "compositeImage": {
                "id": "08da84c0-5544-411d-be1a-7c1745a15073",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5036247b-b696-409b-913b-deee458d50de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cce321a8-3932-4557-8c58-e8537562cad1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5036247b-b696-409b-913b-deee458d50de",
                    "LayerId": "8f357d51-e4b1-4bce-b0ac-b7766d688767"
                }
            ]
        },
        {
            "id": "92064dca-a915-46f3-944c-aa75e87da8c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a972cb30-b248-4c3a-8abe-35ee238b686d",
            "compositeImage": {
                "id": "28104fc4-8a19-40fc-8b07-4de27f46eeee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92064dca-a915-46f3-944c-aa75e87da8c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc5d2cbe-730d-4c7d-ac8e-31217273448b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92064dca-a915-46f3-944c-aa75e87da8c6",
                    "LayerId": "8f357d51-e4b1-4bce-b0ac-b7766d688767"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8f357d51-e4b1-4bce-b0ac-b7766d688767",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a972cb30-b248-4c3a-8abe-35ee238b686d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}