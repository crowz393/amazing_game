{
    "id": "2974f4a5-af65-4df8-9332-caa4a565bb76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d1b53be-c008-41ea-9bdb-4d646e5cc4b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2974f4a5-af65-4df8-9332-caa4a565bb76",
            "compositeImage": {
                "id": "3425fff0-f714-43e3-8058-5b7ce907ce7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d1b53be-c008-41ea-9bdb-4d646e5cc4b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f642570-b959-43f8-ae64-7d88b26b1b5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d1b53be-c008-41ea-9bdb-4d646e5cc4b3",
                    "LayerId": "79d9dc10-ef29-4e3d-b588-c2b2367b10cf"
                }
            ]
        },
        {
            "id": "62b59df9-44f9-4a5e-a87f-7b553f590f80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2974f4a5-af65-4df8-9332-caa4a565bb76",
            "compositeImage": {
                "id": "c9c4d71e-9855-4a92-8229-5526535b5250",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62b59df9-44f9-4a5e-a87f-7b553f590f80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96e8b4ef-9cea-47d9-8191-8b91e0ec9edd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62b59df9-44f9-4a5e-a87f-7b553f590f80",
                    "LayerId": "79d9dc10-ef29-4e3d-b588-c2b2367b10cf"
                }
            ]
        },
        {
            "id": "5bdec363-5e04-4f7b-b4c2-bd1b5c4fda3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2974f4a5-af65-4df8-9332-caa4a565bb76",
            "compositeImage": {
                "id": "310af262-4955-4aed-8b9d-533a4d671388",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bdec363-5e04-4f7b-b4c2-bd1b5c4fda3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e63158e-2b0d-4b22-831a-8c2a708f4652",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bdec363-5e04-4f7b-b4c2-bd1b5c4fda3b",
                    "LayerId": "79d9dc10-ef29-4e3d-b588-c2b2367b10cf"
                }
            ]
        },
        {
            "id": "ab271f95-6a1a-40f6-bdae-2d4c944d3590",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2974f4a5-af65-4df8-9332-caa4a565bb76",
            "compositeImage": {
                "id": "f99d0c6f-29af-46e5-8bf0-599f80a8be8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab271f95-6a1a-40f6-bdae-2d4c944d3590",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "943dfcd1-910e-4081-9859-3fe8a3c8c342",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab271f95-6a1a-40f6-bdae-2d4c944d3590",
                    "LayerId": "79d9dc10-ef29-4e3d-b588-c2b2367b10cf"
                }
            ]
        },
        {
            "id": "0fd64451-f37a-46be-ac76-5ac25b17348e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2974f4a5-af65-4df8-9332-caa4a565bb76",
            "compositeImage": {
                "id": "5bfb0f83-33c7-4aab-9b36-52fb44c5ff06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fd64451-f37a-46be-ac76-5ac25b17348e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fba74eb4-91d1-458a-a9e4-e9e8be98601d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fd64451-f37a-46be-ac76-5ac25b17348e",
                    "LayerId": "79d9dc10-ef29-4e3d-b588-c2b2367b10cf"
                }
            ]
        },
        {
            "id": "b616795b-b3ac-4fef-bace-6de98f0f8370",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2974f4a5-af65-4df8-9332-caa4a565bb76",
            "compositeImage": {
                "id": "95bdedbd-0d9a-457d-b509-1f2f35c03469",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b616795b-b3ac-4fef-bace-6de98f0f8370",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e3f9811-390d-4b7b-be6c-0a2616c78aae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b616795b-b3ac-4fef-bace-6de98f0f8370",
                    "LayerId": "79d9dc10-ef29-4e3d-b588-c2b2367b10cf"
                }
            ]
        },
        {
            "id": "6e31b26a-0bad-4f8c-a610-4b53144428c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2974f4a5-af65-4df8-9332-caa4a565bb76",
            "compositeImage": {
                "id": "8cf93513-5dfd-492e-9e3a-0ff8a288d6e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e31b26a-0bad-4f8c-a610-4b53144428c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8f5ba8b-9d42-4154-82e6-a4209921537a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e31b26a-0bad-4f8c-a610-4b53144428c4",
                    "LayerId": "79d9dc10-ef29-4e3d-b588-c2b2367b10cf"
                }
            ]
        },
        {
            "id": "f55c6b86-4090-41fd-8359-0cd64c97d85f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2974f4a5-af65-4df8-9332-caa4a565bb76",
            "compositeImage": {
                "id": "c7e347ff-c7cb-4226-bd9c-8b0c4398867b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f55c6b86-4090-41fd-8359-0cd64c97d85f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1460551c-ade7-4f08-9673-5f5265242961",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f55c6b86-4090-41fd-8359-0cd64c97d85f",
                    "LayerId": "79d9dc10-ef29-4e3d-b588-c2b2367b10cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "79d9dc10-ef29-4e3d-b588-c2b2367b10cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2974f4a5-af65-4df8-9332-caa4a565bb76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}